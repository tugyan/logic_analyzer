#
#

from PyQt5 import QtCore, QtGui, QtWidgets
from Units import Units
import math

class TimeAxis(QtWidgets.QWidget):

  def __init__(self):
    super(TimeAxis, self).__init__()

    self.signal_len = 30
    self.zoom = 50
    self.freq = 1000
    self.gap = 200
    self.setSize()
    self.back_color = QtGui.QColor(240,240,240)
    self.plot_color = QtCore.Qt.black

  def paintEvent(self, qpe):

    qp = QtGui.QPainter()
    qp.begin(self)
    self.drawWidget(qp)
    qp.end()


  def drawWidget(self, qp):
    z = self.zoom
    g = self.gap
    u = Units()

    font = QtGui.QFont('Serif', 10, QtGui.QFont.Light)
    qp.setFont(font)

    qp.setPen(self.back_color)
    qp.setBrush(self.back_color)
    ss = self.size()
    h = ss.height()
    w = ss.width()
    qp.drawRect(0, 0, w, h)
    qp.drawRect(0, 0, ss.width(), ss.height())

    pen = QtGui.QPen(self.plot_color, 1, QtCore.Qt.SolidLine)
    qp.setPen(pen)
    qp.setBrush(QtCore.Qt.NoBrush)

    nice_num = int(math.log(1/self.freq/z)/math.log(10))
    g = (10**nice_num)*self.freq*z*100

    if g > 400:
      g /= 10
    elif g < 40:
      g *= 10

    left = self.visibleRegion().boundingRect().left()
    right = self.visibleRegion().boundingRect().right()

    small_num, small_pre = u.convert(1/self.freq/z*g)
    small_num = round(small_num)



    for i in range( int(left / g), int(right / g) + 1 ):

      if i == 0:
        pass

      elif i%10 != 0:
        qp.drawLine(i*g, h-15, i*g, h)
        qp.drawText(i*g+2, h-5, '+' + str(int(small_num*(i%10))) + small_pre + 's')

      else:
        number, prefix = u.convert(i/self.freq/z*g)
        number = round(number)
        qp.drawLine(i*g, h-20, i*g, h)
        qp.drawText(i*g, h-20, str(number) + prefix + 's')
        number = int(number)

#      if i/10*g > left and i/10*g < right:
#
#        number, prefix = u.convert((10**nice_num)*i/10)
#        number = int(number)
#
#        if i%10 == 0:
#          qp.drawLine(i/10*g, 25, i/10*g, 95)
#
#          if i != 0:
#            qp.drawText(i/10*g, 20, str(number) + prefix + 's')
#
#        else:
#          qp.drawLine(i/10*g, 45, i/10*g, 95)
#          qp.drawText(i/10*g, 45, str(number%10) + prefix + 's')



    #    is_five = False
    #
    #    print('g before: ' + str(g))
    #
    #    while g < min_g or g > max_g:
    #      if g > max_g:
    #        if not is_five:
    #          g /= 4
    #        else:
    #          g /= 2.5
    #
    #      else:
    #        if is_five:
    #          g *= 4
    #        else:
    #          g *= 2.5
    #
    #      is_five = not is_five
    #
    #    print('g after: ' + str(g))

    # Debug
    # print('g: ' + str(g))



    # Some underflow issues

    # number, prefix = u.convert(10**nice_num)



    # Prints number of line
    # print(self.signal_len*z/50)


################################
#  # Change gap with scroll
################################
#  def wheelEvent(self, e):
#
#    num_degree = e.angleDelta()
#
#    if not num_degree.isNull():
#      self.gap += num_degree.y() / 8.0
#      # Minimum gap: 50
#      if self.gap < 50:
#        self.gap = 50
#      # Maximum gap: 200
#      elif self.gap > 200:
#        self.gap = 200
#
#    self.update()
#    print(self.gap)
#
#    e.accept()


  # Add cursors
  # def mousePressEvent(self, e):


  def setZoom(self, z):

    self.zoom = z
    self.setSize()
    self.update()

  def changeLen(self, l):
    self.signal_len = l
    self.update()

  def setSize(self):

      self.setFixedSize(self.zoom*self.signal_len+40, 50)


  def setFreq(self, f):
    self.freq = f
