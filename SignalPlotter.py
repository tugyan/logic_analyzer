from PyQt5 import QtCore, QtGui, QtWidgets
from Units import Units
import math


class SignalPlotter(QtWidgets.QWidget):
    def __init__(self, signal, freq):
        super(SignalPlotter, self).__init__()

        self.signal = signal
        self.signal_changed = True
        self.zoom = 50
        self.prev_zoom = self.zoom
        self.freq = freq
        self.setSize()
        self.back_color = QtGui.QColor(40, 40, 40)
        self.plot_color = QtCore.Qt.black
        self.setMouseTracking(True)
        self.position = []

        self.measure = []

    def paintEvent(self, qpe):
        qp = QtGui.QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()

    def drawWidget(self, qp):
        z = self.zoom
        u = Units()
        p = self.position

        font = QtGui.QFont('Serif', 10, QtGui.QFont.Light)
        metrics = qp.fontMetrics()
        qp.setFont(font)

        qp.setPen(self.back_color)
        qp.setBrush(self.back_color)
        ss = self.size()
        qp.drawRect(0, 0, ss.width(), ss.height())

        pen = QtGui.QPen(self.plot_color, 1, QtCore.Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(QtCore.Qt.NoBrush)

        if self.signal_changed or (self.prev_zoom != z):
            p.clear()
            self.signal_changed = False
            self.prev_zoom = z
            for i in range(0, len(self.signal)):
                p.append(i * z)
                p.append(self.signal[i] * -35 + ss.height() - 5)
                p.append((i + 1) * z)
                p.append(self.signal[i] * -35 + ss.height() - 5)

        ratio = self.rect().width() / len(self.signal)

        left = self.visibleRegion().boundingRect().left()
        right = self.visibleRegion().boundingRect().right()

        pos_right = math.ceil(right / ratio * 2)

        while (pos_right * 2 + 3) > len(p):
            pos_right -= 1

        for i in range(int(left / ratio), pos_right):
            qp.drawLine(p[i * 2], p[i * 2 + 1], p[i * 2 + 2], p[i * 2 + 3])

        if self.measure:
            qp.drawLine((self.measure[0] + 1) * z, ss.height() - 45, self.measure[1] * z, ss.height() - 45)
            qp.drawLine((self.measure[0] + 1) * z, ss.height() - 48, (self.measure[0] + 1) * z, ss.height() - 42)
            qp.drawLine(self.measure[1] * z, ss.height() - 48, self.measure[1] * z, ss.height() - 42)

            middle = (self.measure[0] + 1 + self.measure[1]) / 2 * z

            time = (self.measure[1] - self.measure[0] - 1) / self.freq
            # print('Time: ' + str(time))
            num, pre = u.convert(time)
            # print(num)
            # print(pre)
            num = round(num)

            fw = metrics.width(str(num) + pre + 's')
            qp.drawText(middle - fw / 2, ss.height() - 55, str(num) + pre + 's')

###
#    plot = QtGui.QPolygon(QtCore.QRect(0, 0, self.size().width(), 15), closed = False)
#    plot.setPoints(position)
#    qp.drawPolygon(plot)
###

    def getSignalLen(self):
        return len(self.signal)

    def setZoom(self, z):
        self.zoom = z
        self.setSize()
        self.update()

    def setSize(self):
        new_width = self.zoom * len(self.signal) + 20
        self.setFixedSize(new_width, 70)

    def setFreq(self, f):
        self.freq = f

    def setColor(self, back, plot):
        self.back_color = back
        self.plot_color = plot

    def mouseMoveEvent(self, e):
        x = e.x()
        curr_index = int(x / self.zoom)

        if curr_index >= len(self.signal):
            return

        i = curr_index - 1
        while i > 0 and self.signal[i] == self.signal[curr_index]:
            i -= 1
        if i < 0:
            self.measure = []
            return

        j = curr_index + 1
        while j < len(self.signal) and self.signal[j] == self.signal[curr_index]:
            j += 1

        if j >= len(self.signal):
            self.measure = []
            return

        self.measure = [i, j]
        self.update()
        # print(self.measure)
