import serial
from SerialCommands import Cmd


class SerialComm(serial.Serial):
    def __init__(self):
        super(SerialComm, self).__init__()
        self.config_dict = self.get_settings()

    def connect_device(self, dev_addr):
        try:
            self.setPort(dev_addr)
            if not self.is_open:
                self.open()

        except serial.SerialException as err:
            raise err

    def change_device(self, dev_addr):
        self.close()
        return self.connect_device(dev_addr)

    def get_device_info(self, arg):
        self.write(Cmd.START_CMD)

        self.write(Cmd.GET_MAX_SAMP_RATE)
        self.dev_mem = self.read()

        self.write(Cmd.GET_TOT_CH_NUM)
        self.ch_num = self.read()

    def startSampling(self):
        self.write(b"\xFE")
        return self.read(size=20000)


# ser = serial.Serial("/dev/ttyACM0")
# ser.write(bytes([6, 2, 3, 4]))
# data = ser.read(4)
# print(data)
# ser.close()
